#include <iostream>
#include <string>

#include "Plateau.hpp"
#include "Jeu.hpp"
#include "GCManager.hpp"
#include "Sys.hpp"

using namespace std;


int main(int argc, char* argv[]){
    SYS::clearScreen();
    cout << "*******  BIENVENUE DANS L EDITEUR DE JEU GACRE  *******" << endl << endl;
    GCM _gcm;
    switch(argc){
        case 1:
            cout << "Usage: gc fichier.board pour l'éditeur de niveau" << endl;
            cout << "gc fichier.game fichier.board [...] pour la création d'un jeu" << endl;
            break;
        case 2:{
            string pName= string(argv[1]);
            //if(SYS::getExtension(argv[i]) != "board"){
            //    cout << "Veuillez selectionner un plateau valide. ( fichier.board )" << endl;
            //    return 1;
            //}
            Plateau *p= _gcm.importPlateauFromTXT(pName);
            _gcm.setMode(p);
            p->show();
            _gcm.exportPlateauToTXT(p);
            // EDIT NIVEAU
            break;
        }
        default:{
            vector<Plateau> plateaux={};
            Plateau *p;
            string jeuName= argv[1];
            //if(SYS::getExtension(argv[i]) != "board"){
            //    cout << "Veuillez selectionner un plateau valide. ( fichier.board )" << endl;
            //    return 1;
            //}
            for(int i=2; i<argc; i++){
                p= _gcm.importPlateauFromTXT(argv[i]);
                plateaux.push_back( *p );
            }
            Jeu *j= new Jeu(jeuName, plateaux);
            _gcm.setMode(j);
            j->show();
            _gcm.exportJeuToTXT(j);
            break;
        }
    }

    //cout << argc << endl;
    // GCM _gcm;
    // Jeu *j= _gcm.importJeuFromTXT("Mehdi's Game");
    // j->show();
    //_gcm.createGame();
    //_gcm.showGame();
    //_gcm.exportJeuToTXT();
    //Plateau p= _gcm.createPlateau();
    //p.setPlayerPosition(2, 3);
    //p.show();
    //Plateau *p= _gcm.importFromTXT("board.txt");
    //p->show();
    //_gcm.exportToTXT(p, "board2.txt");
    //ClearScreen();

    return 0;
}