#include "Plateau.hpp"
#include "Piece.hpp"
#include "Pieces.hpp"

#include <iostream>
#include <string>
#include <fstream>
#include <time.h>

using namespace std;

Plateau::Plateau(const Plateau &p){
    width= p.width; height= p.height;
    plateau_name= p.plateau_name;
    _id= p._id;
    
    board= new Case*[width+2];
    player= Player();

    for(int x=0; x <= width+1; x++)
        board[x]= new Case[height+2];

    int c;
    for(int x=0; x <= width+1; x++){
        for(int y=0; y <= height+1; y++){
            c= (p.board[x][y]).getType();
            board[x][y].setX(x); board[x][y].setY(y);
            if( c == ELT::JOUEUR )
                setPlayerPosition(x, y);
            else
                putPiece( c, x, y );                
        }
    }

}

Plateau::Plateau(int w, int h): width(w < MAX_WIDTH ? w : MAX_WIDTH), height(h < MAX_HEIGHT ? h : MAX_HEIGHT), plateau_name("Plateau sans nom"){
    init_plateau();
}

Plateau::Plateau(int w, int h, string pName): width(w < MAX_WIDTH ? w : MAX_WIDTH), height(h < MAX_HEIGHT ? h : MAX_HEIGHT), plateau_name(pName){
    init_plateau();
}

Plateau::Plateau(int w, int h, string pName, int pId): width(w < MAX_WIDTH ? w : MAX_WIDTH), height(h < MAX_HEIGHT ? h : MAX_HEIGHT), plateau_name(pName){
    init_plateau();
    _id= pId;
}

Plateau::~Plateau(){
    for(int x=0; x<=width+1; x++)
        delete[] board[x];
    delete[] board;
}


/*
    But: Initialisation du tableau avec les murs délimitant le plateau 
    Sortie : Aucune
*/
void Plateau::init_plateau(){

    cout << "*******  Construction du plateau  *******" << endl << endl;
    player= Player();
    _id= rand()%10000;

    board= new Case*[width+2];


    for(int x=0; x <= width+1; x++)
        board[x]= new Case[height+2];
    
    for(int x=0; x <= width+1; x++){
        for(int y=0; y <= height+1; y++){
            board[x][y].setX(x); board[x][y].setY(y);
            if(is_side_board(x, y))
                putPiece( new Mur(), x, y );                
        }
    }
    board[1][1].putPiece(&player);
}

/*
    But: (x,y) est une position valide à l'interieur du plateau ? 
    Sortie : Vrai / Faux
*/
bool Plateau::is_in_board(int x, int y){
    return (
        y >= 1 && y-height-1 < 0 &&
        x >= 1 && x-width-1 < 0 );
}

/*
    But: (x,y) est une position au bord du plateau ? 
    Sortie : Vrai / Faux
*/
bool Plateau::is_side_board(int x, int y){
    return (
        x == 0 || x== width+1 ||
        y==0 || y==height+1 );
}


/*
    But: (x,y) est la position du joueur sur le plateau ?
    Sortie : Vrai / Faux
*/
bool Plateau::is_player_position(int x, int y){
    return ( player.getX() == x && player.getY() == y );
}

/*
    But: Mettre la piece p en position (x,y) sur le plateau?
    Sortie : Vrai / Faux
*/
void Plateau::putPiece(Piece* p, int x, int y){
    //removePiece( board[x][y].getPiece() );
    if(p->isType(ELT::JOUEUR))
        setPlayerPosition(x, y);
    else if( (p->isType(ELT::PORTE_O) || p->isType(ELT::PORTE_F) || p->isType(ELT::MUR)) && is_side_board(x,y) )
        board[x][y].putPiece(p);
    else if( !(p->isType(ELT::PORTE_O) || p->isType(ELT::PORTE_F) ) && is_in_board(x,y) )
        board[x][y].putPiece(p);
}

/*
    But: Mettre la piece représenté par le char c en position (x,y) sur le plateau?
    Sortie : Vrai / Faux
*/
void Plateau::putPiece(int c, int x, int y){
    //removePiece( board[x][y].getPiece() );
    Piece *p;
    switch(c){
        case ELT::JOUEUR: p= new Player(); break;
        case ELT::MUR: p=new Mur(); break;
        case ELT::MONSTRE: p=new Monstre(); break;
        case ELT::DIAMANT: p=new Diams(); break;
        case ELT::PORTE_O: p=new Porte(true); break;
        case ELT::PORTE_F: p=new Porte(false); break;
        case ELT::CHARGEUR: p=new Chargeur(); break;
        case ELT::VIDE: default: p= new Vide(); break;
    };

    if(p->isType(ELT::JOUEUR))
        setPlayerPosition(x, y);
    else if( (p->isType(ELT::PORTE_O) || p->isType(ELT::PORTE_F) || p->isType(ELT::MUR)) && is_side_board(x,y) )
        board[x][y].putPiece(p);
    else if( !(p->isType(ELT::PORTE_O) || p->isType(ELT::PORTE_F) ) && is_in_board(x,y) )
        board[x][y].putPiece(p);
    else{
        delete p;
        board[x][y].putPiece( new Vide() );
    }
    
}

/*
    But: Supprimer une pièce du plateau
    Sortie : Vrai / Faux
*/
void Plateau::removePiece(int x, int y){
    if(is_side_board(x, y))
        putPiece( new Mur(), x, y );
    else if( !player.isPosition(x, y) )
        putPiece( new Vide(), x, y);
}

/*
void Plateau::removePiece(Piece* p){
    if(p->isType(ELT::MUR)){
        Mur *m= dynamic_cast<Mur*>(p);
        removeMur(*m);
    }
    else if(p->isType(ELT::PORTE_O) || p->isType(ELT::PORTE_F)){
        Porte *m= dynamic_cast<Porte*>(p);
        removePorte(*m);
    }
}

void Plateau::removeMur(Mur p){
    murs.erase(std::remove(murs.begin(), murs.end(), p), murs.end());
}
void Plateau::removePorte(Porte p){
    //portes.erase(std::remove(portes.begin(), portes.end(), p), portes.end());
}

*/


/*
    But: p est le nouveau nom de plateau
    Sortie : Aucune
*/
void Plateau::set_name(string p){
    plateau_name= p;
}

/*
    But: (x,y) mod (width+1, height+1) est la nouvelle position du joueur
    Sortie : Aucune
*/
void Plateau::setPlayerPosition(int x, int y){
    if(is_side_board(x,y) || !is_in_board(x,y) )
        return;
    int x1= x % (width+1);  x1= (x1 == 0 ? 1 : x1);
    int y1= y % (height+1); y1= (y1 == 0 ? 1 : y1);
    
    if(player.isIngame()){
        int x_prev= player.getX();
        int y_prev= player.getY();

        board[x_prev][y_prev].putPiece(new Vide());
    }
        

    board[x1][y1].putPiece( &player );
}


/*
    Sortie : Retourne la largeur du plateau
*/
int Plateau::get_width(){
    return width;
}

/*
    Sortie : Retourne la hauteur du plateau
*/
int Plateau::get_height(){
    return height;
}

/*
    Sortie : Retourne la hauteur du plateau
*/
int Plateau::get_id(){
    return _id;
}

/*
    Sortie : Retourne la hauteur du plateau
*/
string Plateau::get_name(){
    return plateau_name;
}

/*
    But: Preview du plateau vierge de taille width x height
    Sortie : Aucune
*/
void Plateau::preview(int width, int height){
    char asc;
    for(int y=0; y<= height+1; y++){
        for(int x=0; x<=width+1; x++){
            if(x == 0 || x== width+1 || y==0 || y==height+1)
                asc= ELT::MUR;
            else if(x == 1 && y == 1)
                asc= ELT::JOUEUR;
            else
                asc= ELT::VIDE;
            if(x!=width+1)
                cout << asc << " ";
            else
                cout << asc;
        }
        cout << endl;
    }
    cout << endl;
}

/*
    But: Affichage des informations ainsi que du tableau lui même
    Sortie : Aucune
*/
void Plateau::show(){
    cout << "*******  " << plateau_name << "  *******" << endl << endl;
    cout << "Taille du tableau : " << width << " x " << height << endl << endl;

    char asc;
    for(int y=0; y<= height+1; y++){
        for(int x=0; x <= width+1; x++){
            asc= board[x][y].getType();
            if(x!=width+1)
                cout << asc << " ";
            else
                cout << asc;
        }
        cout << endl;
    }
    cout << endl;
}

bool operator==(Plateau p1, Plateau p2){
     return p1._id == p2._id;
}

// ostream& operator<<(ostream& out, Plateau p){
//     p.show();
//     return out;
// }