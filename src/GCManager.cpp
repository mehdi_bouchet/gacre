#include <stdio.h>
#include <iostream>
#include <string>
#include <cctype>

#include "GCManager.hpp"
#include "Plateau.hpp"
#include "Jeu.hpp"
#include "Sys.hpp"

using namespace std;

GCM::GCM(){
    mode= _MODE::INIT;
    srand(time(NULL));
    SYS::createDir(BOARD_DIR);
    SYS::createDir(GAMES_DIR);

    plateau= nullptr;
    jeu= nullptr;
}

GCM::~GCM(){
    mode= _MODE::EXIT;

    delete plateau;
    delete jeu;
    
    plateau= nullptr;
    jeu= nullptr;
}
/*
    But: Change le mode en mode Plateau
    Sortie : Vide
*/

void GCM::setMode(Plateau* p){
    plateau= p;
    mode= _MODE::MODE_PLATEAU;
}

/*
    But: Change le mode en mode Jeu
    Sortie : Vide
*/

void GCM::setMode(Jeu* j){
    jeu= j;
    mode= _MODE::MODE_JEU;
}

/*
    But: Change le mode en mode Jeu
    Sortie : Vide
*/

void GCM::setMode(int m){
    switch(m){
        case _MODE::MODE_JEU: mode= _MODE::MODE_JEU; break;
        case _MODE::MODE_PLATEAU: mode= _MODE::MODE_PLATEAU; break;
    }
}
Plateau GCM::createPlateau(const string name){
    int w,h;
    bool isOk= false;
    char confirm;
    string line, plateau_name;
    Plateau *p;

    do{
        cout << endl;
        cout << "*******  Taille du plateau  *******" << endl << endl;

        cout << "Largeur ( max: " << MAX_WIDTH << " ): ";
        cin >> w;
        cout << "Hauteur ( max: " << MAX_HEIGHT << " ): ";
        cin >> h;
        cout << endl << endl;

        w= w < MAX_WIDTH ? w : MAX_WIDTH;
        h= h < MAX_HEIGHT ? h : MAX_HEIGHT;

        cout << "*******  Preview du plateau  *******" << endl << endl;
        Plateau::preview(w, h);

        cout << "Un plateau de cette dimension vous convient-il ? (y/n) : ";
        cin.ignore();
        getline(std::cin, line);
        
        confirm= toupper(line.at(0));
        isOk= (confirm == 'Y');
        
        cout << endl;
    }while(!isOk);

    isOk= false;
    plateau_name= name;

    do{
        if(plateau_name == ""){
            cout << "Le nom du plateau ? : ";
            getline(std::cin, plateau_name);
            cout << endl << endl;
        }


        cout << "*******  " << plateau_name << "  *******" << endl << endl;
        Plateau::preview(w, h);

        cout << "Ce plateau vous convient-il ? (y/n) : ";
        line.clear();
        getline(std::cin, line);
        
        confirm= toupper(line.at(0));
        isOk= (confirm == 'Y');
        if(!isOk) plateau_name= "";

        cout << endl;
    }while(!isOk);

    p= new Plateau(w, h, plateau_name);

    return *p;
}
Plateau GCM::createPlateau(){
    return createPlateau("");
}

void GCM::createGame(){
    int n_plateaux;
    bool isOk= false;
    char confirm;
    string line, jeu_name;

    jeu= new Jeu();

    do{
        SYS::clearScreen();
        cout << "*******  Creer un Jeu  *******" << endl << endl;

        cout << "Le nom du Jeu ? :  ";
        getline(std::cin, jeu_name);
        
        do{
            cout << "Combien de plateaux seront présents dans votre Jeu ? ( 0-6 ): ";
            cin >> n_plateaux;
        
            isOk= (n_plateaux >= 0 && n_plateaux <=6);
        }while(!isOk);
            

        cin.ignore();
        cout << endl << "*******  "<<jeu_name<<" ( " << n_plateaux << " niveaux" << " )  *******" << endl << endl;
        cout << "Ce jeu vous convient-il ? (y/n) : ";
        getline(std::cin, line);

        confirm= toupper(line.at(0));
        isOk= (n_plateaux >= 0 && n_plateaux <=6 && confirm == 'Y');
        
        cout << endl;
    }while(!isOk);

    jeu->setName(jeu_name);
    SYS::clearScreen();

    for(int i=0; i<n_plateaux;i++){
        //p= createPlateau();
        SYS::clearScreen();
        cout << "*******  " << jeu_name << "  *******" << endl;
        cout << "*******  Plateau ( " << to_string(i+1) << "/" << to_string(n_plateaux) << " )  *******" << endl << endl;
        jeu->addPlateau( createPlateau() );
    }

    setMode(_MODE::MODE_JEU);
}

/*
    But: Import du Plateau au format TXT
    Sortie : Le plateau du ficher name s'il existe. Sinon un plateau vide 10x10;
*/
Plateau* GCM::importPlateauFromTXT(const string name){
    string path= BOARD_DIR;
    Plateau *p;
    string pName;


    path.append(name+".board");
    ifstream pstream(path.c_str());

    if(!pstream){
        cout << "Plateau inexistant. Création du plateau " << name << "." << endl << endl;
        p= new Plateau( createPlateau(name) );
        return p;
    }

    int w, h, pId;
    int x=0, y=0;
    bool pass= false;
    char c;

    getline(pstream, pName);
    pstream >> pId;
    pstream >> w;
    pstream >> h;

    cout << "*******  Import du Plateau " << pName <<" ( "<< w << "x" << h << " )  *******" << endl << endl;
    
    p= new Plateau(w, h, pName, pId);
    pstream.ignore(256,'\n');
    while( pstream.get(c) ){
        if( c == '\n'){
            y++;
            x= 0;
            pass= false;
            continue;
        }
        if(pass){
            pass= false;
            continue;
        }
        
        p->putPiece(c, x, y);
        pass= true;
        x++;        
    }
    return p;
}

/*
    But: Export du Plateau p au format TXT
    Sortie : Vrai / Faux en fonction du succès de l'opération
*/
bool GCM::exportPlateauToTXT(Plateau* p, const string pth){
        
    string plateau_name= p->get_name();
    string path= pth;
    path.append(plateau_name+".board");

    ofstream pstream( path.c_str());

    if(!pstream)
        return false;

    char asc;
    cout << "*******  Export de " << plateau_name << " au format  *******" << endl << endl;
    pstream << p->plateau_name << endl;
    pstream << p->get_id() << endl;
    pstream << p->width << endl;
    pstream << p->height << endl;
    
    for(int y=0; y<= p->height+1; y++){
        for(int x=0; x <= p->width+1; x++){
            asc= p->board[x][y].getType();
            if(x!=p->width+1)
                pstream << asc << " ";
            else
                pstream << asc;
        }
        pstream << endl;
    }
    pstream.close();
    cout << "*******  Export du plateau " << plateau_name << " termine  *******" << endl << endl;

    return true;
}

/*
    But: Export du Plateau p au format TXT
    Sortie : Vrai / Faux en fonction du succès de l'opération
*/
bool GCM::exportPlateauToTXT(Plateau* p){
    return exportPlateauToTXT(p, BOARD_DIR);
}

/*
    But: Export du plateau chargé dans le GCM au format TXT
    Sortie : Vrai / Faux en fonction du succès de l'opération
*/
bool GCM::exportPlateauToTXT(){
    return exportPlateauToTXT(plateau);
}

/*
    But: Import du Plateau au format TXT depuis le dossier GAME_DIR/games/
    Sortie : Le plateau du ficher name s'il existe. Sinon un plateau vide 10x10;
*/
Jeu* GCM::importJeuFromTXT(const string name){
    string path= GAMES_DIR;
    string pathImportPlateau= "../"+path;
    

    cout << "*******  Import  *******" << endl;

    path.append(name+"/play.game");
    ifstream jstream(path.c_str());

    if(!jstream)
        return nullptr;
    
    Jeu *j;
    string jName, pTriggerName;
    int numP;

    jstream >> numP;
    jstream.ignore(256,'\n');
    getline(jstream, jName);
    cout << "*******  Import du Jeu " << jName << "  *******" << endl << endl;

    j= new Jeu(jName);
    Plateau *p;
    pathImportPlateau.append(jName+"/");

    for(int i=0; i<numP; i++){
        getline(jstream, pTriggerName);
        p= importPlateauFromTXT(pTriggerName);
        j->addPlateau(*p);
    }
    return j;
}

/*
    But: Export du Plateau au format TXT
    Sortie : Vrai / Faux en fonction du succès de l'opération
*/
bool GCM::exportJeuToTXT(Jeu* jeu){
    if(mode != _MODE::MODE_JEU)
        return false;

    string jeu_name= jeu->jeu_name;
    string path= GAMES_DIR+jeu_name+"/";
    SYS::createDir(path);
    ofstream jstream( path+"play.game" );

    if(!jstream)
        return false;

    SYS::clearScreen();
    cout << "*******  Export du jeu " << jeu_name <<"  *******" << endl << endl;
    Plateau *p;
    jstream << jeu->getSize() << endl;
    jstream << jeu->getName() << endl;

    for(int i=0; i<jeu->getSize(); i++){
        exportPlateauToTXT(jeu->getPlateau(i));
        jstream << (jeu->getPlateau(i))->get_name() + ".board" << endl;
    }
    cout << "*******  Export du jeu " << jeu->getName() << " termine  *******" << endl << endl;

    jstream.close();
    return true;
}

/*
    But: Export du Plateau au format TXT
    Sortie : Vrai / Faux en fonction du succès de l'opération
*/
bool GCM::exportJeuToTXT(){
    return exportJeuToTXT(jeu);
}

void GCM::showGame(){
    SYS::clearScreen();
    jeu->show();
}