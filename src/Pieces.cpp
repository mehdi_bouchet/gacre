#include "Pieces.hpp"

Player::Player(){ type= ELT::JOUEUR; description= "Oueurj"; piece_c= nullptr; }
Player::~Player(){ piece_c= nullptr; }

Monstre::Monstre(){ type= ELT::MONSTRE; description= "Streumons"; piece_c= nullptr; }
Monstre::~Monstre(){ piece_c= nullptr; }

Mur::Mur(){ type= ELT::MUR; description= "Reumus"; piece_c= nullptr; }
Mur::~Mur(){ piece_c= nullptr; }

Diams::Diams(){ type= ELT::DIAMANT; description= "Diams"; piece_c= nullptr; }
Diams::~Diams(){ piece_c= nullptr; }

Chargeur::Chargeur(){ type= ELT::CHARGEUR; description= "Ceurchars"; piece_c= nullptr; }
Chargeur::~Chargeur(){ piece_c= nullptr; }

Porte::Porte(){ open= false; type= ELT::PORTE_F; description= "Reupors Fermée"; piece_c= nullptr; }
Porte::Porte(bool isOpen){ 
    open= isOpen; 
    type= (isOpen ? ELT::PORTE_O : ELT::PORTE_F); 
    description= (isOpen ? "Reupors Ouverte" : "Reupors Fermée"); 
    piece_c= nullptr; 
}
Porte::~Porte(){ piece_c= nullptr; }
void Porte::toggle(){ open= true; type= ELT::PORTE_O; description= "Reupors Ouverte"; piece_c= nullptr; }
bool Porte::isOpen(){ return open; }

Vide::Vide(){ type= ELT::VIDE; description= "Vide"; piece_c= nullptr; }
Vide::~Vide(){ piece_c= nullptr; }