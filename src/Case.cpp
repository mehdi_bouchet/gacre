#include "Case.hpp"
#include "Piece.hpp"
#include "Pieces.hpp"

Case::Case(): x(1), y(1){ piece= new Vide(); piece->setCase(this); }
Case::Case(int xpos, int ypos): x(xpos), y(ypos){ piece= new Vide();  piece->setCase(this); }
Case::Case(Piece &p, int xpos, int ypos): x(xpos), y(ypos){ piece= &p; piece->setCase(this); }
Case::~Case(){ 
    //delete piece; 
    piece= nullptr; }

int Case::getX(){ return x; }
int Case::getY(){ return y; }

int Case::getType(){ return piece->getType(); }
bool Case::isType(int x){ return piece->isType(x); }

Piece* Case::getPiece(){ return piece; }
void Case::putPiece(Piece* p){
    if(!piece->isType(ELT::JOUEUR))
        delete piece;
    piece= p; 
    piece->setCase(this); 
}


void Case::setX(int xpos){ x= xpos; }
void Case::setY(int ypos){ y= ypos; }
        