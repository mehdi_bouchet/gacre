#include "Piece.hpp"

using namespace std;

Piece::~Piece(){}

int Piece::getX(){
    return piece_c->getX();
}
int Piece::getY(){
    return piece_c->getX();
}

int Piece::getType(){
    return type;
}

bool Piece::isType(int x){
    return type == x;
}

bool Piece::isIngame(){
    return ingame;
}

bool Piece::isPosition(int x, int y){
    return piece_c->getX() == x && piece_c->getY() == y;
}

void Piece::setCase(Case* c){
    piece_c= c;
    ingame= (c == nullptr ? false : true);
}

const Case* Piece::getCase(){
    return piece_c;
}

bool operator==(Piece& lhs, Piece& rhs)
{
    return lhs.getX() == rhs.getX() && lhs.getY() == rhs.getY() && lhs.getType() == rhs.getType();
}