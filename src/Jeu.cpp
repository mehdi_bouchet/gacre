#include "Jeu.hpp"
#include "Piece.hpp"

#include <iostream>
#include <string>

using namespace std;

Jeu::Jeu(){
    plateaux= {};
    jeu_name= "Untitled";
}
Jeu::Jeu(string name){
    plateaux= {};
    jeu_name= name;
}
Jeu::Jeu(string name, vector<Plateau> pls){
    jeu_name= name;
    plateaux= {};
    for(int i=0; i< pls.size(); i++)
        plateaux.push_back(pls.at(i));
}

vector<Plateau>* Jeu::getPlateaux(){
    return &plateaux;
}

Plateau* Jeu::getPlateau(){
    return &plateaux.at(plateaux.size()-1);
}

Plateau* Jeu::getPlateau(int i){
    return &plateaux.at(i);
}

void Jeu::swappPlateau(int i1, int i2){
    std::swap(plateaux[i1], plateaux[i2]);
}

void Jeu::replacePlateau(Plateau p, int i){
    vector<Plateau>::iterator position = std::find( plateaux.begin(), plateaux.end(), plateaux[i] );
    if ( position == plateaux.end() ) {
        return;
        //throw NoSuchElement();
    }
    *position = p;
}

string Jeu::getName(){
    return jeu_name;
}

int Jeu::getSize(){
    return plateaux.size();
}
void Jeu::addPlateau(Plateau p){
    plateaux.push_back(p);
}

void Jeu::setName(string const name){
    jeu_name= name;
}
bool Jeu::removePlateau(){
    if(plateaux.size() <= 0)
        return false;
    plateaux.pop_back();
    return true;
}
bool Jeu::removePlateau(int i){
    if(plateaux.size() >= i)
        return false;
    plateaux.erase(plateaux.begin() + i);
    return true;
}

bool Jeu::removePlateau(Plateau p){
    for (vector<Plateau>::iterator it = plateaux.begin(); it != plateaux.end(); ++it) {
        if (it->get_id() == p.get_id()) {
            plateaux.erase(it);
            return true;
        }
    }
    return false;
}

void Jeu::show(){
    int i;
    string plateau_name;
    cout << "*******  AFFICHAGE DU JEU " << jeu_name << "  *******" << endl << endl;
    for (vector<Plateau>::iterator plateau= plateaux.begin(); plateau != plateaux.end(); ++plateau) {
        i= plateaux.size() - ( plateaux.end() - plateau) + 1;
        plateau_name= plateau->get_name();
        cout << "*******  " << plateau_name << " - " << to_string(i) << "/" << to_string(plateaux.size()) <<"  *******" << endl << endl;
        plateau->show();
    }
}