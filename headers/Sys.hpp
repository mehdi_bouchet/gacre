#ifndef SYS_HPP
#define SYS_HPP

namespace SYS{
  void clearScreen();
  bool createDir(std::string const pth);
  std::string getExtension(std::string file);
};

#endif