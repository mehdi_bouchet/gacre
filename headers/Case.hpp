#ifndef CASE_HPP
#define CASE_HPP

class Piece;
class Vide;
class Mur;
class Player;
class Case {
    protected:
        Piece *piece;
        int x, y;

    public:
        Case();
        Case(int xpos, int ypos);
        Case(Piece &p, int xpos, int ypos);
        ~Case();

        int getX();
        int getY();

        Piece* getPiece();
        int getType();
        bool isType(int);
        void putPiece(Piece* p);

        void setX(int xpos);
        void setY(int ypos);
        
};

#endif