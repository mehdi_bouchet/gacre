#ifndef PIECES_HPP
#define PIECES_HPP

#include <string>
#include <stdio.h>

#include "Piece.hpp"

class Player : public Piece{
    public: Player();
    ~Player();
};

class Monstre : public Piece{
    public: Monstre();
    ~Monstre();
};

class Mur : public Piece{
    public: Mur();
    ~Mur();
};

class Chargeur : public Piece{
    public: Chargeur();
    ~Chargeur();
};

class Porte : public Piece{
    private:
        bool open;
    public: Porte();
    public: Porte(bool);
    void toggle();
    bool isOpen();
    ~Porte();
};

class Diams : public Piece{
    public: Diams();
    ~Diams();
};

class Vide : public Piece{
    public: Vide();
    ~Vide();
};

#endif