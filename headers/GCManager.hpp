#ifndef GCM_HPP
#define GCM_HPP

#include <iostream>
#include <string>

#include "Pieces.hpp"

#define BOARD_DIR "./boards/"
#define GAMES_DIR "./games/"

enum _MODE{
    EXIT=-1,
    INIT=0,
    MODE_JEU=1,
    MODE_PLATEAU=2,
};
/*
    gc fichier.board c’est l’éditeur de niveau. 
    gc .game .board... permet de créer un jeu constitué des board déjà
    existants.
*/
class Plateau;
class Jeu;

class GCM{
    private:
        Plateau* plateau;
        Jeu* jeu;
        int mode;
        
    public:
        GCM();
        ~GCM();
        void _init();
        void setMode(Plateau*);
        void setMode(Jeu*);
        void setMode(int);
        Plateau createPlateau();
        Plateau createPlateau(const std::string);
        void createGame();
        
        Plateau* importPlateauFromTXT(const std::string);
        bool exportPlateauToTXT(Plateau*, const std::string);
        bool exportPlateauToTXT(Plateau* p);
        bool exportPlateauToTXT();
        
        Jeu* importJeuFromTXT(const std::string);
        bool exportJeuToTXT(Jeu *);
        bool exportJeuToTXT();

        void showGame();
        void showPlateau();
};

#endif