#ifndef JEU_HPP
#define JEU_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Plateau.hpp"

using namespace std;

class Plateau;
class GCM;
class Jeu {
    private:
        vector<Plateau> plateaux;
        string jeu_name;

    public:
        Jeu();
        Jeu(string);
        Jeu(string, vector<Plateau>);

        string getName();
        void swappPlateau(int, int);
        void replacePlateau(Plateau, int);
        vector<Plateau> *getPlateaux();
        Plateau* getPlateau();
        Plateau* getPlateau(int);

        int getSize();

        void addPlateau(Plateau);

        void setName(string const);

        bool removePlateau();
        bool removePlateau(int);
        bool removePlateau(Plateau);

        void show();

        friend Jeu* GCM::importJeuFromTXT(const std::string);
        friend bool GCM::exportJeuToTXT(Jeu*);
        
};
#endif